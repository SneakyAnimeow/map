local formulas = {}

function formulas.distanceBetween(x1,y1, x2, y2)
  return math.abs(x1 - x2) + math.abs(y1 - y2)
end

return formulas