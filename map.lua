local map = {
  box = {
  { x=0, y=0},	 --box1
	{ x=50, y=0},	 --box2
	{ x=100, y=0},	 --box3
	{ x=150, y=0},	 --box4
	{ x=200, y=0},	 --box5
	{ x=250, y=0},	 --box6
	{ x=300, y=0},	 --box7
	{ x=350, y=0},	 --box8
	{ x=400, y=0},	 --box9
	{ x=450, y=0},	 --box10
	{ x=0, y=50},	 --box11
	{ x=50, y=50},	 --box12
	{ x=100, y=50},	 --box13
	{ x=150, y=50},	 --box14
	{ x=200, y=50},	 --box15
	{ x=250, y=50},	 --box16
	{ x=300, y=50},	 --box17
	{ x=350, y=50},	 --box18
	{ x=400, y=50},	 --box19
	{ x=450, y=50},	 --box20
	{ x=0, y=100},	 --box21
	{ x=50, y=100},	 --box22
	{ x=100, y=100},	 --box23
	{ x=150, y=100},	 --box24
	{ x=200, y=100},	 --box25
	{ x=250, y=100},	 --box26
	{ x=300, y=100},	 --box27
	{ x=350, y=100},	 --box28
	{ x=400, y=100},	 --box29
	{ x=450, y=100},	 --box30
	{ x=0, y=150},	 --box31
	{ x=50, y=150},	 --box32
	{ x=100, y=150},	 --box33
	{ x=150, y=150},	 --box34
	{ x=200, y=150},	 --box35
	{ x=250, y=150},	 --box36
	{ x=300, y=150},	 --box37
	{ x=350, y=150},	 --box38
	{ x=400, y=150},	 --box39
	{ x=450, y=150},	 --box40
	{ x=0, y=200},	 --box41
	{ x=50, y=200},	 --box42
	{ x=100, y=200},	 --box43
	{ x=150, y=200},	 --box44
	{ x=200, y=200},	 --box45
	{ x=250, y=200},	 --box46
	{ x=300, y=200},	 --box47
	{ x=350, y=200},	 --box48
	{ x=400, y=200},	 --box49
	{ x=450, y=200},	 --box50
	{ x=0, y=250},	 --box51
	{ x=50, y=250},	 --box52
	{ x=100, y=250},	 --box53
	{ x=150, y=250},	 --box54
	{ x=200, y=250},	 --box55
	{ x=250, y=250},	 --box56
	{ x=300, y=250},	 --box57
	{ x=350, y=250},	 --box58
	{ x=400, y=250},	 --box59
	{ x=450, y=250},	 --box60
	{ x=0, y=300},	 --box61
	{ x=50, y=300},	 --box62
	{ x=100, y=300},	 --box63
	{ x=150, y=300},	 --box64
	{ x=200, y=300},	 --box65
	{ x=250, y=300},	 --box66
	{ x=300, y=300},	 --box67
	{ x=350, y=300},	 --box68
	{ x=400, y=300},	 --box69
	{ x=450, y=300},	 --box70
	{ x=0, y=350},	 --box71
	{ x=50, y=350},	 --box72
	{ x=100, y=350},	 --box73
	{ x=150, y=350},	 --box74
	{ x=200, y=350},	 --box75
	{ x=250, y=350},	 --box76
	{ x=300, y=350},	 --box77
	{ x=350, y=350},	 --box78
	{ x=400, y=350},	 --box79
	{ x=450, y=350},	 --box80
	{ x=0, y=400},	 --box81
	{ x=50, y=400},	 --box82
	{ x=100, y=400},	 --box83
	{ x=150, y=400},	 --box84
	{ x=200, y=400},	 --box85
	{ x=250, y=400},	 --box86
	{ x=300, y=400},	 --box87
	{ x=350, y=400},	 --box88
	{ x=400, y=400},	 --box89
	{ x=450, y=400},	 --box90
	{ x=0, y=450},	 --box91
	{ x=50, y=450},	 --box92
	{ x=100, y=450},	 --box93
	{ x=150, y=450},	 --box94
	{ x=200, y=450},	 --box95
	{ x=250, y=450},	 --box96
	{ x=300, y=450},	 --box97
	{ x=350, y=450},	 --box98
	{ x=400, y=450},	 --box99
	{ x=450, y=450},	 --box100
  }
}
map.size = {
  w = 50,
  h = 50,
}
function map.drawBox(n, texture)
  --debug--love.graphics.rectangle("line", map.box[n].x, map.box[n].y, map.size.w, map.size.h)
  --debug--love.graphics.printf(n, map.box[n].x, map.box[n].y, 30, "center")
  love.graphics.draw(texture, map.box[n].x, map.box[n].y)
end

return map