local textures = {
  tree = love.graphics.newImage("textures/tree.png"),
  water = love.graphics.newImage("textures/water.png"),
  lilypad = love.graphics.newImage("textures/lilypad.png"),
  wood = love.graphics.newImage("textures/wood.png"),
  hero = love.graphics.newImage("textures/hero.png")
}
return textures

--textures.quad = love.graphics.newQuad(0, 0, 50, 50)