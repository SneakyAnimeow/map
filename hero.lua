local hero = {
  pos = {x=0,y=0},
  size = 25,
}

function hero.set(x,y)
  hero.x = x
  hero.y = y
end

function hero.controls()
  if love.keyboard.isDown("left") and hero.pos.x > 0 then
    hero.pos.x = hero.pos.x - 1
  elseif love.keyboard.isDown("right") and hero.pos.x < love.graphics.getWidth()-hero.size then
    hero.pos.x = hero.pos.x + 1
  end
  if love.keyboard.isDown("up") and hero.pos.y > 0 then
    hero.pos.y = hero.pos.y - 1
  elseif love.keyboard.isDown("down") and hero.pos.y < love.graphics.getHeight()-hero.size then
    hero.pos.y = hero.pos.y + 1
  end
end

function hero.draw()
  love.graphics.draw(textures.hero, hero.pos.x, hero.pos.y, 0, 0.5,0.5)
end

function hero.getPos()

end

return hero