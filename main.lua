function love.load()
  properties = require("properties")
  map = require("map")
  textures = require("textures")
  frames = require("frames")
  hero = require("hero")
  properties.load()
end

function love.update()
  hero.controls()
end

function love.draw()
  frames.currentFrame()
end