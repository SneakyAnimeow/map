local sea = {}
require("hero")
require("formulas")
sea.lilypads = {1,6,7,11,15,16,17,18,21,25,28,31,35,36,37,38,41,42,43,55,56,57,58,61,63,65,71,73,75,76,77,78,81,83,85,92,95,96,97,98}
sea.redlilypads = {44,45,54,64}
function sea.draw()
  for key in pairs(map.box) do
    map.drawBox(key, textures.water)
  end
  for key, value in pairs(sea.lilypads) do
    map.drawBox(value, textures.lilypad)
  end
  for key, value in pairs(sea.redlilypads) do
    map.drawBox(value, textures.wood)
  end
  hero.draw()
  --love.graphics.print(hero.getPos())
end
function sea.events()
end
return sea