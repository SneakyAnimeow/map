sea = require("sea")
local frames = {
  ID = 0,
}

function frames.currentFrame()
  if frames.ID==0 then
    sea.draw()
    sea.events()
  end
end

function frames.block()
  return true
end

return frames