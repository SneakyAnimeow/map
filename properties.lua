local properties = {
  window = {
    width = 500,
    heigth = 500,
    fullscreen = false,
    title = "map",
  }
}

function properties.load()
  love.window.setMode(properties.window.width, properties.window.heigth, {fullscreen = properties.window.fullscreen})
  love.window.setTitle(properties.window.title)
end

return properties